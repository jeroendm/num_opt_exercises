# Numerical optimization: exercises in python 2#

### What is this repository for? ###

Python code solution for the exercises of [this course](https://onderwijsaanbod.kuleuven.be/syllabi/e/H04U1CE.htm#activetab=doelstellingen_idp540384).
During the sessions we solve the exercises with matlab.
This was an ideal opportunity to learn python (numpy).

### How do I get set up? ###

Install python 2.7, numpy/scipy and matplotlib

### Who do I talk to? ###

* Jeroen De Maeyer: jeroen.demaeyer@kuleuven.be