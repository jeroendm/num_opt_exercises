# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 09:17:40 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as pp

#def nls(x, ys, ts):
#
#    m = len(ys)
#    v = 0
#    
#    for i in range(m)
#        y = ys[i, :]
#        t = ts[i]
#        
#        x7 = (x[3] - t)**2
#        x8 = x7 / x[4]
#        x9 = exp(-x8)
#        x10 = x[2] * x9;
#        x11 = cos(x[6] * t)
#        x12 = x(5) * x11
#        x13 = x10 + x12
#        x14 = x[1] + x13
#        x15 = (y - x14)^2**2
#        
#        v = v+x15;
#        
#    return v
    

data = np.loadtxt("nls_data.txt", skiprows=1)
# save data in 2D numpy arrays
ts = data[:, 0][:, None]
ys = data[:, 1][:, None]

pp.plot(ts, ys)