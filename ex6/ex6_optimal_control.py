# -*- coding: utf-8 -*-
"""
Created on Thu Dec  1 14:35:17 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as plt
import functions as cs
import scipy.optimize as opt

# fix issue of cut off labels
# http://stackoverflow.com/questions/6774086/why-is-my-xlabel-cut-off-in-my-matplotlib-plot
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
    
# test ode and integrator
    
N = 50
Ts = 0.2
h = Ts
n_steps = int( Ts / h )

u = np.zeros(N-1) # input
#u[10:15] = -1

# initialize state
x0 = np.array([[1], [0]])
x = np.zeros((2, N))
x[:, 0, None] = x0

# simulate system dynamics for Ts * (N-1) seconds   
# ===============================================
for i in range(N-1):
    x[:, i+1, None] = cs.rk4integrator(x[:, i, None], u[i], h, n_steps, cs.ode)
 

t = np.arange(0, Ts * N, Ts)   
figure()
subplot(211)
plot(t[:-1], u)
xlabel('Time [s]')
title('input')
subplot(212)
plot(t, x[0, :], 'b', t, x[1, :], 'g')
legend(['Angle', 'Velocity'])
xlabel('Time [s]')
title('State vector')

# sanity check on contraints
# ==========================
#w = np.vstack((x, np.append(u, 9)))
#w = w.flatten('F')[:-1]
#res = cs.g_fun(w, h, n_steps)
# everthing zero except the last element
# this is the boundary condition at the end, x[0, -1] = -0.973.., not zero
#slack = cs.h_fun(w)


# define optimal control problem
# ==============================
w0 = zeros((3 * N - 1, 1))
w0[0] = 1
##w0 = w
#u_max = 0.55
#p_max = 1
#v_max = 0.4
u_max = 1
p_max = 1
v_max = 1
#
con = ({'type': 'eq', 'fun': lambda x: cs.g_fun(x, h, n_steps)},
{'type': 'ineq', 'fun': lambda x: cs.h_fun(x, p_max, v_max, u_max)} )

res = opt.minimize(cs.f_fun, w0, method='SLSQP', constraints=con)

w = res.x
u = w[2:-2:3]
p = w[0:-1:3]
v = w[1::3]
t = np.arange(0, Ts * N, Ts)   

figure()
subplot(211)
plot(t[:-1], u, 'g.-')
xlabel('Time [s]')
title('input')
subplot(212)
plot(t, p, 'b', t, v, 'g')
legend(['Angle', 'Velocity'])
xlabel('Time [s]')
title('State vector')

# formulate objective for Gauss-Newton method


def F_fun(w):
    N = (len(w) + 1) / 3 # number of steps
    J = np.zeros(3 * N - 1)
    J[2::3] = 1
    J = np.diag(J)
    
    return np.dot(J, w[:, None])

