# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 17:34:04 2016
Converted code pendulum.m fron exercise files to python
@author: jeroen
"""

import numpy as np

def cost(x0, u, du):
    """
    Cost function of pendulum
    For specific Q and R and system
    Q = [10 0; 0 10]
    R = 1
    g = 9.81
    h = 0.2 (time step)
    c = 2 (damping coefficient)
    
    Parameters
    ----------
    xo : (2, 1) ndarray
        Initial state of the pendulum
    u : (N, 1) ndarray
        input vector for N simulation steps
    du : (N, 1) ndarray
        derivative of input u to time du/dt
    
    Output
    ------
    
    tuple with two elements
    
    J : scalar
        total cost sum(xk'Qxk + uk'Ruk) for k=1...N
    dJ : scalar
        Derivative of cost to input dJ / du
    X : (N, 2) ndarray
        state vectors
    """
    #system parameters
    g, h, c = 9.81, 0.2, 3.0
    gh, ch = g*h, c*h
    
    
    # number of time steps simulated
    N = len(u)
    X = np.zeros((2, N))
    
    # initial point
    J = 0
    dJ = 0
    xk1 = x0[0, 0]
    xk2 = x0[1, 0]
    
    for k in range(N):
        
        # calculate state k based on k-1
        z1=h*xk2;
        z2=np.sin(xk1)
        z3=-gh*z2
        z4=-ch*xk2
        
        z5=h*u[k, 0]
        dz5 = h * du[k, 0]
        
        z6=z3+z4
        z7=z5+z6
        dz7 = dz5        
        
        xk1=xk1+z1
        
        xk2=xk2+z7
        dxk2 = dz7
        
        # calculate cost for step k
        z8=xk1**2
        z9=xk2**2
        dz9 = 2 * xk2 * dxk2        
        
        z10=10*z8
        z11=10*z9
        dz11 = 10 * dz9
        
        z12=u[k, 0]**2
        dz12 = 2 * u[k, 0] * du[k, 0]
        
        Jstep=z10+z11
        dJstep = dz11        
        
        Jstep=Jstep+z12
        dJstep = dJstep + dz12
        
        J += Jstep
        dJ += dJstep
        
        # store state k in X
        X[0, k] = xk1
        X[1, k] = xk2
        
    return J, dJ, X

