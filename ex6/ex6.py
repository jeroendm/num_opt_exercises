# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 15:56:55 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as pp
import pendulum as cs

x0 = np.array([0.1, 0]).reshape(2, 1)
#u = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])[:, None]
u = np.zeros((30, 1))
du = np.zeros((30, 1))
du[0, 0] = 1
#u[10:15, 0] = 0.5

J, dJ, X = cs.cost(x0, u, du)

# finite difference comparison
Jdu, dJdu, Xdu = cs.cost(x0, u + 1e-8, du)
dJfin = (Jdu-J) / 1e-8
print dJ, dJfin

theta = X[0, :]
dtheta = X[1, :]

pp.plot(theta, '-.', dtheta, '-.')
pp.legend(['theta', 'dtheta'])