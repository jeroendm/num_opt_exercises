# -*- coding: utf-8 -*-
"""
Created on Thu Dec  1 16:31:16 2016

@author: jeroen
"""
import numpy as np

def rk4integrator(x_k, u, h, n_steps, ode):
    """
    Runge-Kutta integration for differential equation ode
    """
    
    assert x_k.shape == (2, 1), "x_k must have shape (2, 1)"
    
    x_int = x_k
    for k in range(n_steps):
        k1 = h * ode(x_int, u)
        k2 = h * ode(x_int + k1 / 2.0, u)
        k3 = h * ode(x_int + k2 / 2.0, u)
        k4 = h * ode(x_int + k3, u)
        x_int = x_int + 1.0 / 6.0 * (k1 + 2*k2 + 2*k3 + k4)
        
    return x_int
    
def ode(x, u):
    """
    Ordinary differential equation for pendulum
    """
    return np.vstack(( x[1], -np.sin(x[0]) + u ))
    
def f_fun(w):
    u = w[3:-3:3]
    return 0.5 * sum(u**2)

def f_fun2(w):
    return 0.5 * sum(w**2)
      
    
def g_fun(w, h, n_steps):
    u = w[2:-2:3]
    p = w[0:-1:3]
    v = w[1::3]
    x = np.vstack((p, v))
    
    N = (len(w) + 1) / 3 # number of steps
    
    # Dynamic constraints
    g = np.zeros((2, N-1))
    for i in range(N-1):
        g[:, i, None] = x[:, i+1, None] - rk4integrator(x[:, i, None], u[i], h, n_steps, ode)
    
    g =  g.flatten()
    
    # Boundary constraints
    g = np.append(g, [ p[0] - 1, v[0], p[-1] ])
    
    return g
    
def h_fun(w, p_max, v_max, u_max):
    u = w[2:-2:3]
    p = w[0:-1:3]
    v = w[1::3]
#    x = np.vstack((p, v))
    
    h = np.array([])
    h = np.append(h,  p + p_max)
    h = np.append(h, -p + p_max)
    h = np.append(h,  v + v_max)
    h = np.append(h, -v + v_max)
    h = np.append(h,  u + u_max)
    h = np.append(h, -u + u_max)
    
    return h
    
    
def test_g_fun():
    h = 0.2
    n_steps = 2
    x = range(11)
    con = g_fun(x, h, n_steps)
    print x, con