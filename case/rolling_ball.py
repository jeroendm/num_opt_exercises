# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:09:01 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as plt
#from rk4 import rk4
import optistack as opt
import casadi as cas

# Physicql parameters of system
r = 1 # ball radius
m = 10 # ball mass
I = 2.0/5.0 * m * r**2 # ball mass moment of inertia


def ball_model(x, v, u):
    """Ball_model equation of motion of rolling ball on inclined surface
    Where x is position, v is speed and u is inclination angle"""
    
    return [v, -9.81 / 1.4 / m * np.sin(u)]
    
#np.vectorize(ball_model)

def euler(f, x, v, u, h):
    """Euler integration step for given differential equation
    dt is time step"""
    [dx, dv] = f(x, v, u)
    
    return [x + dx * h, v + dv * h]
    
def rk4(f, x, v, u, h):
    """Runge-Kutta integration stap, order 4"""
    k1 = f(x, v, u)
    k2 = f(x + h/2*k1[0], v + h/2*k1[1], u)
    k3 = f(x + h/2*k2[0], v + h/2*k2[1], u)
    k4 = f(x + h*k3[0], v + h*k3[1], u)
    
    dx =  k1[0] + 2*k2[0] + 2*k3[0] + k4[0]
    dv =  k1[1] + 2*k2[1] + 2*k3[1] + k4[1]
    
    return [x + dx * h/6, v + dv * h/6]
    
# simulation parameters
h = 0.05 # simulation time step
T = 5
N = int(T / h) # simulation points

w = 0.1 # importance of small control signal

u_max = 20 * np.pi / 180
u_min  = -20 * np.pi / 180

t = np.arange(0, N*h, step=h)
x_path = np.sin(np.pi / T * t)


x = opt.optivar(N)
v = opt.optivar(N)
u = opt.optivar(N)

# Objective
#J = cas.sum_square(x - x_path) #+ w * cas.sum_square(u**2)
J = cas.norm_2(x - x_path)

# constraints
g = [x[0] == 0.5, v[0] == 0]#, u_min <= u,  u <= u_max]
u.setLb(u_min)
u.setUb(u_max)

# dynamic constraints
for i in range(1, N):
    [xn, vn] = euler(ball_model, x[i-1], v[i-1], u[i-1], h)
    g.append(x[i] == xn)
    g.append(v[i] == vn)
    
sol = opt.optisolve(J, g)

xs = opt.optival(x)
vs = opt.optival(v)
us = opt.optival(u)

"""
xs = np.zeros(N)
vs = np.zeros(N)
us = np.sin(2 * np.pi * t / 5)

for i in range(1, N):
    [xs[i], vs[i]] = rk4(ball_model, xs[i-1], vs[i-1], us[i-1], h)
"""

plt.figure()
plt.subplot(311)
plt.plot(t, x_path, '-', t, xs, '-')
plt.legend(['goal', 'real'])

plt.subplot(312)
plt.plot(t, us / np.pi * 180, '-')
plt.title('input signal')

plt.subplot(313)
plt.plot(t, vs, '-')
plt.title('speed')


