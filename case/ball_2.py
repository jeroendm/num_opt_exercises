# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 09:51:54 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as plt

# Physicql parameters of system
l = 0.05 # ball radius
m = 0.2 # ball mass
I = 2.0/5.0 * m * l**2 # ball mass moment of inertia
c = 1.0 / (1.0 + I / l**2) # constant to simplify equations

g = 9.81 # gravity constant

def ball_model(x, v, u, du):
    """Ball_model equation of motion of rolling ball on inclined surface
    Where x is position on surface, v is speed and u is inclination angle"""
    
    return [v, c * (x * du**2 - g * m * np.sin(u))]
    
def rk4(f, x, v, u, du, h):
    """Runge-Kutta integration stap, order 4"""
    k1 = f(x, v, u, du)
    k2 = f(x + h/2*k1[0], v + h/2*k1[1], u, du)
    k3 = f(x + h/2*k2[0], v + h/2*k2[1], u, du)
    k4 = f(x + h*k3[0], v + h*k3[1], u, du)
    
    dx =  k1[0] + 2*k2[0] + 2*k3[0] + k4[0]
    dv =  k1[1] + 2*k2[1] + 2*k3[1] + k4[1]
    
    return [x + dx * h/6, v + dv * h/6]
    
# simulation parameters
h = 0.02 # simulation time step
T = 5
N = int(T / h) # simulation points
t = np.arange(0, N*h, step=h) # time vector


xs = np.zeros(N)
vs = np.zeros(N)
u_max = 5 * np.pi / 180
us = u_max * np.sin(2 * np.pi * t / 5)
dus = u_max * 2 * np.pi / 5 * np.cos(2 * np.pi * t / 5)

for i in range(1, N):
    [xs[i], vs[i]] = rk4(ball_model, xs[i-1], vs[i-1], us[i-1], dus[i-1], h)


sur = np.linspace(-1.5, 1.5, 20)

plt.figure()
plt.axis([-1, 1, -0.2, 0.2])
plt.ion()

for i in range(N):
    plt.cla()
    plt.axis([-1, 1, -0.2, 0.2])
    surface_x = sur * np.cos(us[i])
    surface_y = sur * np.sin(us[i])
    ball_x = xs[i] * np.cos(us[i]) #- l * np.sin(us[i])
    ball_y = xs[i] * np.sin(us[i]) #+ l * np.cos(us[i])
    plt.plot(0, 0, 'r*')
    plt.plot(surface_x, surface_y, 'b')
    plt.plot(ball_x, ball_y, 'go')
    
    plt.pause(h)


#plt.figure()
#plt.subplot(311)
#plt.plot(t, us, '-', t, dus, '-')
#plt.legend(['input signal', 'du'])
#
#plt.subplot(312)
#plt.plot(t, xs, '-')
#plt.legend(['position'])
#
#plt.subplot(313)
#plt.plot(t, vs, '-')
#plt.title('speed')
