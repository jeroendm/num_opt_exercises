# -*- coding: utf-8 -*-
"""
Created on Sat Nov 19 09:12:58 2016

@author: jeroen

excersise 5 of case session
use first order dynamics with colloquation method
dx/dt = ( x(k+1) - x(k) ) / Dt
"""

import numpy as np
import matplotlib.pyplot as plt
#from rk4 import rk4
import optistack as opt
#import casadi as cas

N = 100      # discretization points
c = 2        # drag coefficient[Ns^2/m^2]
m = 500      # car mass [kg]
d = 1000     # distance to travel[m]
Fmin = -2500 # minimum force[N]
Fmax =  2500 # maximum force [N]
    
x = opt.optivar((2,N + 1))  # state vector with position ans speed
F = opt.optivar((1, N))     # input force
T = opt.optivar()             # motion time

# boundary constraints
con = [x[0, 0] == 0, x[0, -1] == d, x[1, 0] == 0, x[1, -1] == 0] # 4 eq constr
con.append(F <= Fmax) # 100 ic
con.append(F >= Fmin) # 100 ic
con.append(T >= 0) # 1 ic

con.append(x[1, :] >= 0)

#F.setUb(Fmax)
#F.setLb(Fmin)

T.setInit(1)
# dynamic contraints
h = T / N# integration time step

# two times 100 eq constr in loop = 200
for i in range(N):
    # implicit first order diff equations as function of speed
#    con.append( m * (x[1, i + 1] + x[1, i]) / h == F[i] - c * x[1, i]**2  )
    """ bij deze vgl termen van lid veranderen heeft invloed op # iteraties
    time step h in de noemer link zetten geeft een error:
    'Inf detected for output jac_g_x, at nonzero index 4'
    Dit probleem is opgelost door de initiële waarde van T = 1 te zetten,
    zo kan h niet nul worden
    """
    con.append( m * (x[1, i+1] - x[1, i]) / h  == F[i] - c * x[1, i]**2 )
    # integrate speed for position
    con.append( x[0, i+1] == x[0, i] + h * x[1, i] )

# solve problem
sol = opt.optisolve(T, con)

Ts = float(opt.optival(T))
t = np.linspace(0, Ts, N + 1)
t = t.reshape((N+1,)) # reshape returns a vieww !
xs = opt.optival(x)
Fs = opt.optival(F).reshape((N,))

plt.figure()
plt.subplot(311)
plt.plot(t[:-1], Fs, '*-')
plt.title('input force')

plt.subplot(312)
plt.plot(t, xs[0, :].T)
plt.title('position')

plt.subplot(313)
plt.plot(t, xs[1, :].T)
plt.title('speed')

#def rhs(v, F):
#    """ right hand side of first order implicit diff equation
#    m * dv/dt = (F - c*v^2)"""
#    return (F - c * v**2)