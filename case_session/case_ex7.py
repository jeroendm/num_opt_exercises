# -*- coding: utf-8 -*-
"""
Created on Sat Nov 19 09:12:58 2016

@author: jeroen

excersise 7
"""

import numpy as np
import matplotlib.pyplot as plt
#from rk4 import rk4
import optistack as opt
import casadi as cas

N = 101      # discretization points
c = 2        # drag coefficient[Ns^2/m^2]
m = 500      # car mass [kg]
d = 1000.0     # distance to travel[m]
Fmin = -2500 # minimum force[N]
Fmax =  2500 # maximum force [N]

x = np.linspace(0, d, N)
    
b = opt.optivar((1,N))  # state vector with position ans speed
b.setInit(1)
#F = opt.optivar((1, N))     # input force
#T = opt.optivar()             # motion time

#obj = cas.sum2( 1 / (cas.sqrt(b[1:]) + cas.sqrt(b[:-1])) )

obj = 0
for i in range(1, N-2):
    obj = obj + ( 1 / ( cas.sqrt(b[i+1]) + cas.sqrt(b[i]) ) )

dx = d / N
#h = dx / cas.sqrt(b) # niet hier doen, delen door nul vermijden

con = [b[0] == 0, b[-1] == 0]
for i in range(N-1):
     dbi = (b[i + 1] - b[i]) * cas.sqrt(b[i]) / dx # colloquation method
     con.append( (0.5 * m * dbi + c * b[i]) <= Fmax )
     con.append( (0.5 * m * dbi + c * b[i]) >= Fmin )



# solve problem
sol = opt.optisolve(obj, con)

bs = opt.optival(b).reshape((N,))

# calculate time vector
dt = dx / ((bs[:-1] + bs[1:]) / 2)
t = np.cumsum(dt)
t = np.insert(t, 0, 0) # returns copy

# calculate force vector
F = 0.5 * m * (bs[1:] - bs[:-1]) / dt + c * bs[:-1]

plt.plot(t, bs)
plt.figure()
plt.plot(F)