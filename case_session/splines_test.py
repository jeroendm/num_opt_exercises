# -*- coding: utf-8 -*-
"""
Created on Sat Nov 19 13:39:41 2016

@author: jeroen


"""

# use splines to fit and interpolate data
import spline as sp
import numpy as np

import matplotlib.pyplot as plt
import optistack as opt
import casadi as cas

d = 3
#n = 11
#L = 1

B = sp.BSplineBasis([0, 0.5, 1, 1.5, 2], d)


S = sp.Spline(B, [1, 1, 1, 1, 1])