# -*- coding: utf-8 -*-
"""
Created on Sat Nov 19 09:12:58 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as plt
#from rk4 import rk4
import optistack as opt
import casadi as cas

N = 100      # discretization points
c = 2        # drag coefficient[Ns^2/m^2]
m = 500      # car mass [kg]
d = 1000     # distance to travel[m]
Fmin = -2500 # minimum force[N]
Fmax =  2500 # maximum force [N]

def ode(x, u):
    """Equation of motion of the car"""
    return cas.vertcat(x[1, :], (u - c * x[1, :]**2) / m)

""" euler methode gebruiken geeft ongeveer dubbel zo veel iteraties"""
def euler(f, x, u, h):
    """euler integration step, returns x(k+1) from input xk"""
    return x + h * f(x, u)
    
def rk4(f, x, u, h):
    """Runge-Kutta 4the order integration step
    returns x(k+1) from input xk"""
    k1 = ode(x, u)
    k2 = ode(x + h/2*k1, u)
    k3 = ode(x + h/2*k2, u)
    k4 = ode(x + h*k3, u)
    
    return x + h/6 * (k1 + 2*k2 + 2*k3 + k4)
    
x = opt.optivar((2,N + 1))  # state vector with position ans speed
F = opt.optivar((1, N))     # input force
T = opt.optivar()             # motion time

# boundary constraints
con = [x[0, 0] == 0, x[0, -1] == d, x[1, 0] == 0, x[1, -1] == 0]
con.append(F <= Fmax)
con.append(F >= Fmin)
con.append(T >= 0)
"""T >= 0 toevoegen heeft aantal iteraties van 300 tot 68 teruggebracht
"""
con.append(x[1, :] >= 0)
""" v >= 0 toevoegen maakt het probleem onstabiel of zo,
gaat dan to max number of iterations
MAAR met T >= 0 er bij help deze dan zeer eom het aantal iteraties terug
te brengen tot 29 !!"""
#F.setUb(Fmax)
#F.setLb(Fmin)

# dynamic contraints
h = T / N# integration time step
for i in range(N):
    state_next = rk4(ode, x[:, i], F[i], h)
    con.append(x[:, i + 1] == state_next)
    

# solve problem
sol = opt.optisolve(T, con)

Ts = float(opt.optival(T))
t = np.linspace(0, Ts, N + 1)
t = t.reshape((N+1,)) # reshape returns a vieww !
xs = opt.optival(x)
Fs = opt.optival(F).reshape((N,))

plt.figure()
plt.subplot(311)
plt.plot(t[:-1], Fs, '*-')
plt.title('input force')

plt.subplot(312)
plt.plot(t, xs[0, :].T)
plt.title('position')

plt.subplot(313)
plt.plot(t, xs[1, :].T)
plt.title('speed')