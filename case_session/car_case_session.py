# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 13:50:00 2016

@author: jeroen
"""
from sys import path

path.append(r"/home/jeroen/casadi-py27-np1.9.1-v3.1.0-rc1")
path.append(r"/home/jeroen/python-optistack-master")

from optistack import *
import numpy as np

# car model parameters
d = 1000.0     # travelled distance [m]
m = 500.0      # car mass [kg]
c = 2.0        # air drag coeff []
Fmax = 2500.0  # maximum input force [N]
Fmin = -2500.0 # minimum input force[N]

# simulation parameters
N = 100
hn = 1.0 / N # normalized time step

# optimization variables
T = optivar(1)
u = optivar(N)
x = optivar(N+1)
v = optivar(N+1)

# formulate opimization problem
objective = T
constraints = [v >= 0, T >= 0,
               u <= Fmax, u >= Fmin,
               x[0] == 0, x[N] == d,
                v[0] == 0, v[N] == 0,
                x[1:N] == x[0:(N-1)] + hn * T * v[0:(N-1)],
                v[1:N] == v[0:(N-1)] + hn * (1.0 / m) * ( T**2 * u[0:(N-1)] )]

# optimize

x.setInit(zeros(N+1,1))

nlp = optisolve(objective, constraints)

T_sol = optival(T)
u_sol = optival(u)
x_sol = optival(x)
v_sol = optival(v)

import matplotlib.pyplot as plt
import numpy as np

time = np.linspace(0, T_sol, N)

plt.figure()
plt.subplot(311)
plt.plot(u_sol, '*-')
plt.title('Input force')

plt.subplot(312)
plt.plot(x_sol, '*-')
plt.title('Distance')

plt.subplot(313)
plt.plot(v_sol, '*-')
plt.title('Speed')
