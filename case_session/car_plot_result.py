# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 14:23:22 2016

@author: jeroen
"""

from sys import path

path.append(r"/home/jeroen/casadi-py27-np1.9.1-v3.1.0-rc1")
path.append(r"/home/jeroen/python-optistack-master")

from optistack import *

T_sol = optival(T)
u_sol = optival(u)
x_sol = optival(x)
v_sol = optival(v)

import matplotlib.pyplot as plt
import numpy as np

time = np.linspace(0, T_sol, N)

plt.figure()
plt.subplot(311)
plt.plot(time, u_sol, '*-')
plt.title('Input force')

plt.subplot(312)
plt.plot(time, x_sol[:, 0:N], '*-')
plt.title('Distance')

plt.subplot(313)
plt.plot(time, v_sol[:, 0:N], '*-')
plt.title('Speed')