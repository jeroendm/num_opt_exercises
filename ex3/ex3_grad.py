# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 13:42:56 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as pp
import custom_functions as cs
from min_grad import *

N = 100
x = np.linspace(-2, 2, N)
y = np.linspace(-2, 2, N)
xv, yv = np.meshgrid(x, y)

""" easy objective function"""

f = cs.objective_toy(xv, yv)

sl = 0.01
x0 = np.array([1, 1]).reshape(2, 1)
fopt, x_iters, f_iters = min_grad(lambda x: cs.objective_toy(x[0], x[1]), x0, sl)

pp.figure("Easy objective")

pp.subplot(211)
pp.contour(x, y, f, levels=range(1, 40, 3))
pp.xlabel('x')
pp.ylabel('y')
pp.hold(True)
pp.plot(x_iters[0, :], x_iters[1, :], '-o')
pp.hold(False)

pp.subplot(212)
iters = np.arange(f_iters.size)
pp.semilogy(iters, f_iters, '.')
pp.xlabel('Iteration')
pp.ylabel('log10(Function value)')


""" Rosenbrock objective function """
f = cs.objective_rosenbrock(xv, yv)



sl = 0.001
x0 = np.array([-1.5, 1]).reshape(2, 1)
fopt, x_iters, f_iters = min_grad(lambda x: cs.objective_rosenbrock(x[0], x[1]), x0, sl)

pp.figure("Rosenbrock")

pp.subplot(211)
pp.contour(x, y, f, levels=range(1, 100, 5))
pp.xlabel('x')
pp.ylabel('y')
pp.hold(True)
pp.plot(x_iters[0, :], x_iters[1, :], '-o')
pp.hold(False)

pp.subplot(212)
iters = np.arange(f_iters.size)
pp.semilogy(iters, f_iters, '.')
pp.xlabel('Iteration')
pp.ylabel('log10(Function value)')