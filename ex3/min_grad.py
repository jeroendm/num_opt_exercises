# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 20:32:47 2016

@author: jeroen
Matlab files from exercise sessions converted to python
"""

import numpy as np
import custom_functions as cs

def min_grad(fun, x0, sl):
    """Minimize function based on gradient method
    
    input
    fun:   Function (nx1) vector input to scalar output
    x0:    Initial condition
    sl: step length
    
    output
    fopt: optimal solution
    xiter: (n,i) array with x of each itteration step
    f iter: (1, i) array with function value at each itteration step
    """
    
    # convergence parameters
    grad_tol = 1e-4
    max_iters = 100000
    
    x = x0;

    # a log of the iterations
    x_iters = np.zeros((len(x0), max_iters));
    f_iters = np.zeros((1,max_iters));
    
    for k in range(max_iters):
        # store x in the iteration log
        x_iters[:,k][:, None] = x;
        f_iters[:,k] = fun(x);
        
        # check for divergence
        assert np.max(np.abs(x)) < 1e6, 'minimize_grad_desc has diverged, max(abs(x)): {}'.format( np.max(np.abs(x)) )
        
        # evaluate the local gradient
        f, J = cs.finite_difference_jacob(fun, x);
        
        # check for convergence
        if np.linalg.norm(J, np.inf) < grad_tol:
            x_iters = x_iters[:,0:k];
            f_iters = f_iters[0, 0:k];
            
            print "convergence!! after: {} iterations".format(k)
            return f, x_iters, f_iters
        
        # take the gradient descent step
        # search direction
        pk = -J.T;
        
        # step
        x = x + sl * pk;
    
    assert 0==1, 'minimize_grad_desc: max iterations exceeded'
    
    