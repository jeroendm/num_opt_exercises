# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 20:32:47 2016

@author: jeroen
Matlab files from exercise sessions converted to python
"""

import numpy as np
import custom_functions as cs

def min_bfgs(fun, x0, gamma, beta):
    """Minimize function using BFGS-method
    misschien gamma en beta vervangen door *argv
    
    input
    fun:   Function (nx1) vector input to scalar output
    x0:    Initial condition
    sgamma : line search parameter
    beta : line search parameter
    
    output
    fopt: optimal solution
    xiter: (n,i) array with x of each itteration step
    f iter: (1, i) array with function value at each itteration step
    """
    
    # convergence parameters
    grad_tol = 1e-4
    max_iters = 100000
    
    assert x0.shape == (2, 1), "s has wrong dimensions"
    
    x = x0;
    B = np.eye(x.size)
    assert B.shape == (2, 2), "B has wrong dimensions {}".format(B.shape)

    # a log of the iterations
    x_iters = np.zeros((len(x0), max_iters));
    f_iters = np.zeros((1,max_iters));
    
    f, J = cs.finite_difference_jacob(fun, x);
    
    for k in range(max_iters):
        # store x in the iteration log
        x_iters[:,k][:, None] = x;
        f_iters[:,k] = fun(x);
        
        print B
        
        # check for divergence
        assert np.max(np.abs(x)) < 1e6, \
        'minimize_bfsg has diverged, \
        max(abs(x)): {} at iteration {}'.format(np.max(np.abs(x)),  k)
        
        # check for convergence
        if np.linalg.norm(J, np.inf) < grad_tol:
            x_iters = x_iters[:,0:k];
            f_iters = f_iters[0, 0:k];
            
            print "convergence!! after: {} iterations".format(k)
            return f, x_iters, f_iters
        
        # search direction
        pk = np.linalg.solve(-B, J.T)
        
        # execute line search
        x_old = x
        x = cs.line_search(fun, x, J, pk, gamma, beta);
        J_old = J
        
        # evaluate the local gradient
        f, J = cs.finite_difference_jacob(fun, x);
        
        # update hessian estimate
        s = x - x_old
        assert s.shape == (2, 1), "s has wrong dimensions"
        y = (J - J_old).T
        assert y.shape == (2, 1), "y has wrong dimensions"
        B = B - (B * (s * s.T) * B) / (s.T * B * s) + y * y.T / (s.T * y)
    
    assert 0==1, 'minimize_bfsg: max iterations exceeded'
    
    