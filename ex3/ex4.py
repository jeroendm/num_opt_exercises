# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 19:29:13 2016

@author: jeroen
"""

import matplotlib.pyplot as pp
from custom_functions import *

N = 100
x = np.linspace(-10, 10, N)
y = np.linspace(-5, 5, N)
xv, yv = np.meshgrid(x, y)

f = objective_toy(xv, yv)

pp.contour(x, y, f)