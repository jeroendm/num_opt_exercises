# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 08:56:42 2016

@author: jeroen
"""

import numpy as np
import custom_functions as cs

def minimize_cgn(Ffun, eqcon, x0):
    """
    minimize obj(x) using the Constrained Gauss-Newton method
    subjeted to equality constrained eqcon(x) = 0
    
    Parameters
    ----------
    Ffun : function
        error function to calculate objective 0.5 * F.T * F
    eqcon : function
        equality constraind function
    x0 : initial guess
    
    Returns
    -------
    x_opt : optimal value
    x_iters : (n, i) vector containing i times (n,1) iteration vectors
    f_iters : (1, i) vector containing i function values from iterations
    """
    
    # convergence parameters
    grad_tol = 1e-4
    max_iters = 100000
    
    x = x0;
    g0 = eqcon(x0)
    lam = np.zeros(g0.shape)
    B = np.eye(len(x0))

    # a log of the iterations
    x_iters = np.zeros((len(x0), max_iters))
    f_iters = np.zeros((1,max_iters))
    
    for k in range(max_iters):
        # store x in the iteration log
        x_iters[:,k][:, None] = x
        
        # check for divergence
        assert np.max(np.abs(x)) < 1e6, 'minimize_gn has diverged, max(abs(x)): {}'.format( np.max(np.abs(x)) )
        
        # evaluate the local jacobian
        F, J_F = cs.finite_difference_jacob(Ffun, x)
        g, J_g = cs.finite_difference_jacob(eqcon, x)
        J = np.dot(J_F.T, F).T
        
        # check for convergence
        stop_crit = np.vstack((J.T - J_g.T*lam, g))
        f_iters[:,k] = np.linalg.norm(stop_crit, 2)
        
        if  np.linalg.norm(stop_crit, np.inf) < grad_tol:
            x_iters = x_iters[:,0:k]
            f_iters = f_iters[0, 0:k]
            
            print "convergence!! after: {} iterations".format(k)
            x_opt = x
            return x_opt, x_iters, f_iters
        
        
        # search direction
        B = np.dot(J_F.T, J_F)
        assert B.shape == (len(x0), len(x0)), 'hessian approx wrong shape'
        lhs = np.vstack(( np.hstack((B, J_g.T)), np.append(J_g, 0) ))
        sol = np.linalg.solve( lhs, np.vstack((-J.T, -g)) )
        pk = sol[0:2, 0][:, None]
        lam_hat = -sol[2]
        
        # execute line search
        gamma = 0.01
        beta = 0.6
        sigma = 100
        
        der_merit_fun = np.dot(J, pk) - sigma * np.linalg.norm(g, 1)
        x, t = cs.line_search(lambda x: merit_fun(Ffun, eqcon, x, sigma), x, der_merit_fun, pk, gamma, beta)
        lam = lam + t * (lam_hat - lam)
        
        print 'Iteration', k, 'x: ', x
    
    assert 0==1, 'minimize_grad_desc: max iterations exceeded'

def merit_fun(Ffun, eqcon, x, sigma):
    """
    Merit function used to do line search
    """
    F = Ffun(x)
    f = 0.5 * np.dot(F.T, F)
    g = eqcon(x)
    
    return f + sigma * np.linalg.norm(g, 1)

    