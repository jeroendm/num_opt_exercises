# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 19:29:13 2016

@author: jeroen

 Try to work only with two dimensional numpy arrays!
 Alternatief, alleen met matrix werken?
 voor deel dat dit:
     x0 = np.array([1, 1]).reshape(2, 1)
 dit wordt:
     x0 = np.mat('1; 1')
 
"""

import matplotlib.pyplot as pp
import numpy as np
import custom_functions as cs
from minimize_gn import *

# fix issue of cut off labels
# http://stackoverflow.com/questions/6774086/why-is-my-xlabel-cut-off-in-my-matplotlib-plot
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
    
# solution
#=========

data = np.loadtxt("emmision_data.txt", skiprows=1)
# save data in 2D numpy arrays
t = data[:, 0][:, None]
rates = data[:, 1][:, None]

def model_error(x,t,rates):
    """
    Function representing the error between model and measurements
    model r(t) = c * exp(-t / tau)
    
    Parameters
    ----------
    x : numpy.ndarray shape (2, 1)
        model parameters c and tau
    t : numpy.ndarray shape(m, 1)
        m measurements time stamps
    rater : numpy.ndarray shape (m, 1)
        m measurements
    
    Returns
    -------
    (m, 1) array with signed absolute error between model and measurements
    """
    c = x[0]
    tau = x[1]
    
    assert tau != 0, "model parameter tau cannot be zero"
    
    return c * np.exp(-t / tau) - rates
    
def Fobjective(F, x):
    """
    objective is 0.5 * two norm squared
    """
    Fx = F(x)
    return float(0.5 * np.dot(Fx.T, Fx))

# TESTING
#=========================================================================
x0 = np.array([1, 1]).reshape(2, 1)

# test types and shapes
er = model_error(x0, t, rates)
assert er.shape == (len(t), 1), "Ffun has wrong output format"
assert type(Fobjective(lambda x: model_error(x, t, rates), x0)) is float
# test jacobian
Fk, Jk = cs.finite_difference_jacob(lambda x: model_error(x, t, rates), x0)
assert Fk.shape == (len(t), 1)
assert Jk.shape == (len(t), len(x0))

grad_fk = np.dot(Jk.T, Fk)
assert grad_fk.shape == (2, 1), "wrong shape for grad_f"

Ffun = lambda x: model_error(x, t, rates)
pk = np.linalg.solve(-np.dot(Jk.T, Jk), grad_fk);
assert pk.shape == (len(x0), 1)

xk, temp = cs.line_search(lambda x: Fobjective(Ffun, x), x0, grad_fk.T, pk, 0.1, 0.5)
assert xk.shape == (len(x0), 1)


# SOLVING
#=========================================================================

x0 = np.array([1, 1]).reshape(2, 1)
x_opt, x_iters, f_iters = minimize_gn(Fobjective, lambda x: model_error(x, t, rates), x0)

# PLOTTING
#=========================================================================

pp.figure("Emmision data model fitting")
pp.subplot(211)
pp.plot(t, rates, 'b.', t, x_opt[0] * np.exp(-t / x_opt[1]), 'r')
pp.legend(('data', 'model'))
pp.xlabel('Time [s]')
pp.ylabel('Emmision [unit]')

pp.subplot(212)
iters = np.arange(f_iters.size) + 1
pp.semilogy(iters, f_iters, 'bo')
pp.grid(True)
pp.xlabel('Iteration')
pp.ylabel('Objective function value')

# Test functions
# =============
#def test_Ffun():
#    """test the output size of Ffun
#    and plot results
#    """
#    x0 = np.array([1, 1]).reshape(2, 1)
#    er = Ffun(x0, t, rates)
#    assert er.shape == (len(t), 1), "Ffun has wrong output format"
#    
#    pp.figure()
#    pp.plot(t, rates, 'b.', t, np.exp(-t), 'r')
#    
#    pp.figure()
#    pp.plot(t, er, 'b.')
