# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 08:56:42 2016

@author: jeroen
"""

import numpy as np
import custom_functions as cs

def minimize_gn(fun, Ffun, x0):
    """
    minimize using the Gauss-Netwton method
    
    Parameters
    ----------
    fun : function
        objective function in function of F
    Ffun : function
        error function to calculate objective 0.5 * F.T * F
    x0 : initial guess
    
    Returns
    -------
    x_opt : optimal value
    x_iters : (n, i) vector containing i times (n,1) iteration vectors
    f_iters : (1, i) vector containing i function values from iterations
    """
    
    # convergence parameters
    grad_tol = 1e-4
    max_iters = 100000
    
    x = x0;

    # a log of the iterations
    x_iters = np.zeros((len(x0), max_iters))
    f_iters = np.zeros((1,max_iters))
    
    for k in range(max_iters):
        # store x in the iteration log
        x_iters[:,k][:, None] = x
        f_iters[:,k] = fun(Ffun, x)
        
        # check for divergence
        assert np.max(np.abs(x)) < 1e6, 'minimize_gn has diverged, max(abs(x)): {}'.format( np.max(np.abs(x)) )
        
        # evaluate the local gradient
        Fk, Jk = cs.finite_difference_jacob(Ffun, x)
        
        # gradient objective calculated from local gradient Ffun, Jk and Fk
        grad_f = np.dot(Jk.T, Fk)
        assert grad_f.shape == (2, 1), "wrong shape for J_f"
        
        # check for convergence
        if np.linalg.norm(grad_f, np.inf) < grad_tol:
            x_iters = x_iters[:,0:k]
            f_iters = f_iters[0, 0:k]
            
            print "convergence!! after: {} iterations".format(k)
            x_opt = x
            return x_opt, x_iters, f_iters
        
        
        # search direction
        pk = np.linalg.solve(-np.dot(Jk.T, Jk), grad_f)
        
        # execute line search
        gamma = 0.1
        beta = 0.5
        # line search uses jacobian, not gradient!
        x, temp = cs.line_search(lambda x: fun(Ffun, x), x, np.dot(grad_f.T, pk), pk, gamma, beta)
    
    assert 0==1, 'minimize_grad_desc: max iterations exceeded'
    

    