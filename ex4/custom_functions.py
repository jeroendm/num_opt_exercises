# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 14:36:12 2016

@author: jeroen
dimension issue
http://stackoverflow.com/questions/
18438730/how-to-assign-a-1d-numpy-array-to-2d-numpy-array

gaat niet meer super werken omdat arrays and matrix types niet meer consisten
zijn

"""

import numpy as np

def finite_difference_jacob( fun, x0):
    """
    Calculate jacobian of function using forward euler

    Parameters   
    ----------
    fun : python function to calculate gradient of
    x0 : point at which to calculate gradient
    """
    h = 1e-8
    
    x0 = np.array(x0)
    Nx, cols = x0.shape
    
    f0 = fun(x0)
    
    Nf = f0.size
    
    J = np.zeros((Nf, Nx))
    hI = h * np.eye(Nx)
    
    
    for i in range(Nx):
        
        # mmm, not cool, the shape of I[:, i] is (3,) not (3, 1)
        # nog een elegantere oplossing zoeken dan [:, None] of reshape
        J[:, i][:, None] = ( fun(x0 + hI[:, i].reshape((Nx,1))) - fun(x0) ) / h
    
    return f0, J
    # return f0, np.matrix(J)
    
def objective_toy(x, y):
    return x**2 + 10*y**2
    
def objective_rosenbrock(x, y):
    return (1 - x)**2 + 100 * (y - x**2)**2
    
def line_search( fun, x0, Jk, pk, gamma, beta ):
    """
    Line search using Armijo conditions and backtracking
    only 2D ndarrays convention! np.dot for matrix product
    
    IMPORTANT: line search is changes in ex 4
    Jk is passed instead of J
    
    Paraneters
    ----------
    fun : scalar function
    x0 : initial guess         (n, 1)
    Jk : J jacobian of fun at x0, Jk = J.T * pk
    pk : search direction      (n, 1)
    gamma : armijo condition scaling function
    beta : backtracking parameter
    
    Returns
    -------
    trail_x : The new x value, x_trail = x0 + t * pk
     """
    
    # make sure gamma and beta are in reasonable range
    if (gamma <= 0 or 1 <= gamma):
        assert 'gamma must be in (0,1)'
    
    if (beta <= 0 or 1 <= beta):
        assert 'beta must be in (0,1)'
    
    too_many_steps_counter = 1000
    
    # initialize t, evaluate function
    t = 1
    f0 = fun(x0)
    
    trial_x = x0 + t * pk
    while ( np.all(fun(trial_x) > f0 + gamma * t * Jk )):
        t = beta * t     
        trial_x = x0 + t * pk
#        print 'line search cond: ', fun(trial_x), '>', (f0 +gamma * t * Jk), ' and t: ', t
    
        # if the line search takes too many iterations, throw an error
        too_many_steps_counter = too_many_steps_counter - 1
        if too_many_steps_counter == 0:
            assert 1 == 0, 'line search fail - took too many iterations'
    
    return trial_x, t
    
# Test functions
#===============

# test line search
def test_line_search():
    """ Test function """
    def test_function(xyz):
        x = xyz[0]
        y = xyz[1]
        z = xyz[2]
        
        f1 = x*y + np.exp(z)
        f2 = x*np.exp(-2*y) + x*z
        
        return np.vstack((f1, f2))

    """ execute test """
    # try 100 random points, make sure line_search returns somthing
    for k in range(2):
        x0 = np.random.randn(3,1)
        f0, J = finite_difference_jacob(test_function, x0)
        beta = 0.5
        gamma = 0.5
        
        pk = np.array([1, 0, 0]).reshape(3, 1)
        
        xk = line_search(test_function, x0, J, pk, gamma, beta)
        # torelace 1e-4 based on matlab file exercises
    
        print xk
    
    return 0

# test finite_difference_jacob
def test_finite_difference_jacob():
    """ Test function """
    def test_function(xyz):
        x = xyz[0]
        y = xyz[1]
        z = xyz[2]
        
        f1 = x*y + np.exp(z)
        f2 = x*np.exp(-2*y) + x*z
        
        return np.vstack((f1, f2))

    def test_function_jacob(xyz):
        x = float(xyz[0])
        y = float(xyz[1])
        z = float(xyz[2])
        
        # return array, not matrix, as a convention to work with 2D arrays only
        J = np.array([[y, x, np.exp(z)],
                        [np.exp(-2*y) + z, -2*x*np.exp(-2*y), x]])
                        
        return J
    
    #execute test
    #------------
    # try 100 random points, make sure finite_step_jacob gives something
    # close to the analytical solution
    for k in range(50):
        x0 = np.random.randn(3,1)
        f0, J = finite_difference_jacob(test_function, x0)
        J_analytical = test_function_jacob(x0)
        
        # torelace 1e-4 based on matlab file exercises
        err_msg='finite difference jacobian gives the wrong answer :('
        np.testing.assert_allclose(J, J_analytical,
                                   rtol=1e-4,
                                   err_msg=err_msg)
    
    print 'finite difference jacobian seems to be working, yay!'
    
    return 0
    
#test_finite_difference_jacob()
#test_line_search()