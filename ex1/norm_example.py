# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 08:46:12 2016

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as plt

# measurements
x = np.mat('0; 1; 2')
y = np.mat('0; 1; 0')

n = len(x)

# fit model y = a * x + b with y, x two element vecotors and a, b scalars

""" l1-norm
r = y - (a * x + b)
minimize sum(abs(r))
solution is the y-axis where the sum is 1
moving the line op would reduce objective
by d and add the same time increase by 2*d """

y_l1 = np.mat('0; 0; 0')

# Least squares, L2 norm
# minimize (y - (a * x + b)) with vars (a, b)
# formulate as J * (a, b)' = y with J = (x', 1)

J = np.hstack((x, np.ones((n, 1))))

sol = np.linalg.pinv(J) * y
a = np.float(sol[0])
b = np.float(sol[1])

y_l2 = a * x + b

# linf norm
# r = y - (a * x + b)
# minimize max(r) with vars (a, b)
# solution is in between the two y-coordinates extremes
y_linf = np.mat('0.5; 0.5; 0.5');

plt.xkcd()
plt.figure()
plt.title('Fitting with norms')
plt.axis([-0.5, 2.5, -0.5, 1.5])
plt.plot(x, y, 'ko--', x, y_l1, 'bo--', x, y_l2, 'go--', x, y_linf, 'ro--')
plt.xlabel('x')
plt.ylabel('y')
plt.legend( ('Measurements', 'L1-norm', 'L2-norm', 'Linf-norm'), loc = 'lower left', bbox_to_anchor = (1, 0) )

