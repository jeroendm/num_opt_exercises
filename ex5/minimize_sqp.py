# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 08:56:42 2016

@author: jeroen
"""

import numpy as np
import custom_functions as cs
from cvxopt import matrix, base, solvers

def minimize_sqp(obj, eqcon, x0):
    """
    minimize obj(x) using the Newton-Lagrange method
    subjeted to equality constrained eqcon(x) = 0
    
    Parameters
    ----------
    fun : function
        objective function in function of F
    Ffun : function
        error function to calculate objective 0.5 * F.T * F
    x0 : initial guess
    
    Returns
    -------
    x_opt : optimal value
    x_iters : (n, i) vector containing i times (n,1) iteration vectors
    f_iters : (1, i) vector containing i function values from iterations
    """
    
    # convergence parameters
    grad_tol = 1e-4
    max_iters = 1 #100000
    
    x = x0;
    g0 = eqcon(x0)
    lam = np.zeros(g0.shape)
    B = np.eye(len(x0))

    # a log of the iterations
    x_iters = np.zeros((len(x0), max_iters))
    f_iters = np.zeros((1,max_iters))
    
    for k in range(max_iters):
        # store x in the iteration log
        x_iters[:,k][:, None] = x
        
        # check for divergence
        assert np.max(np.abs(x)) < 1e6, 'minimize_gn has diverged, max(abs(x)): {}'.format( np.max(np.abs(x)) )
        
        # evaluate the local jacobian
        f, J = cs.finite_difference_jacob(obj, x)
        g, J_g = cs.finite_difference_jacob(eqcon, x)
        
        # check for convergence
        stop_crit = np.vstack((J.T - J_g.T*lam, g))
        f_iters[:,k] = np.linalg.norm(stop_crit, 2)
        
        if  np.linalg.norm(stop_crit, np.inf) < grad_tol:
            x_iters = x_iters[:,0:k]
            f_iters = f_iters[0, 0:k]
            
            print "convergence!! after: {} iterations".format(k)
            x_opt = x
            return x_opt, x_iters, f_iters
        
        
        # search direction
        # dit deel vervangen door qp
        Pqp = matrix(B)
        qqp = matrix(J.T)
        Aqp = matrix(J_g)
        bqp = matrix(-g)
        
        print Pqp.size, qqp.size, Aqp.size, bqp.size
        
        sol = solvers.qp(Pqp, qqp, A=Aqp, b=bqp)
        
        print sol
        
        lhs = np.vstack(( np.hstack((B, J_g.T)), np.append(J_g, 0) ))
        sol = np.linalg.solve( lhs, np.vstack((-J.T, -g)) )
        pk = sol[0:2, 0][:, None]
        lam_hat = -sol[2]
        
        # execute line search
        x_old = x
        gamma = 0.01
        beta = 0.6
        sigma = 100
        
        der_merit_fun = np.dot(J, pk) - sigma * np.linalg.norm(g, 1)
        x, t = cs.line_search(lambda x: merit_fun(obj, eqcon, x, sigma), x, der_merit_fun, pk, gamma, beta)
        lam = lam + t * (lam_hat - lam)
        
        # Update hessian estimate
        J_old = J
        J_g_old = J_g
        
        f, J = cs.finite_difference_jacob(obj, x)
        g, J_g = cs.finite_difference_jacob(eqcon, x)
        
        Lx_old = J_old.T - J_g_old.T * lam
        Lx = J.T - J_g.T * lam
        
        # update hessian estimate
        s = x - x_old
        assert s.shape == (2, 1), "s has wrong dimensions"
        y = Lx - Lx_old
        assert y.shape == (2, 1), "y has wrong dimensions"
        
        # Powell's trick
        dB1 = np.dot(B, np.dot(s, np.dot(s.T, B))) / np.dot(s.T, np.dot(B, s))
        dB2 = np.dot(y, y.T) / np.dot(s.T, y)
        B = B - dB1 + dB2
        
        B = (B + B.T) / 2.0
        print 'Iteration', k, 'x: ', x
    
    assert 0==1, 'minimize_grad_desc: max iterations exceeded'

def merit_fun(obj, eqcon, x, sigma):
    """
    Merit function used to do line search
    """
    f = obj(x)
    g = eqcon(x)
    
    return f + sigma * np.linalg.norm(g, 1)

    