# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 18:22:02 2016

@author: jeroen
"""

import matplotlib.pyplot as pp
import numpy as np
import custom_functions as cs
from minimize_sqp import *

import scipy.optimize as opt
import cvxopt.solvers as cvx
from cvxopt import matrix

# fix issue of cut off labels
# http://stackoverflow.com/questions/6774086/why-is-my-xlabel-cut-off-in-my-matplotlib-plot
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

# probblem definition
#====================

def obj(x, y):
    """Objective function to minimize"""
    return 0.5 * (x**2 + (y / 2)**2)
    
def eqcon(x, y):
    """Equality constrained function, should be equal to zero"""
    return y - (x - 1)**2 + x - 3

# Solve problem
#====================

x0 = np.array([2.8, 1]).reshape((2, 1))
x_opt, x_iters, f_iters = minimize_sqp(lambda x: obj(x[0], x[1]), lambda x: eqcon(x[0], x[1]), x0)

# Plot solution
#==============  
N = 100
x = np.linspace(-5, 5, N)
y = np.linspace(-5, 8, N)
xv, yv = np.meshgrid(x, y)

f = obj(xv, yv)
g = (x - 1)**2 - x + 3

pp.figure("Easy objective")

pp.subplot(211)
pp.contour(x, y, f)
pp.hold(True)
pp.plot(x, g, 'k')
pp.axis([-5, 5, -5, 8])
pp.xlabel('x')
pp.ylabel('y')
pp.plot(x_iters[0, :], x_iters[1, :], '-o')
pp.hold(False)

pp.subplot(212)
iters = np.arange(f_iters.size)
pp.semilogy(iters, f_iters, '.')
pp.xlabel('Iteration')
pp.ylabel('log10(Function value)')